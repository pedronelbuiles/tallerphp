<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ejercicio 4</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Mono" rel="stylesheet">
    </head>
    <body>
        <div class="container text-center mb-3">
			<h1 class="h1">Ejercicio número 4</h1>
		</div>
		<div class="container">
        	<div class="row">
        		<div class="col-12 text-center">
        			<p>Lea tres números e imprímalos en orden descendente. Resuélvalo utilizando conectivos lógicos y sin ellos.</p>
        		</div>
        	</div>
        </div>
        <section>
        	<!-- Pedir números -->
        	<div class="container">
        		<form action="" method="post">
	        		<div class="row">
	        			<div class="col-12 col-md-6 text-center">
	        				<label for="primerN">Ingrese el primer número</label>
	        			</div>
	        			<div class="col-12 col-md-6">
	        				<input type="number" id="primerN" name="primerN">
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-12 col-md-6 text-center">
	        				<label for="segundoN">Ingrese el segundo número</label>
	        			</div>
	        			<div class="col-12 col-md-6">
	        				<input type="number" id="segundoN" name="segundoN">
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-12 col-md-6 text-center">
	        				<label for="tercerN">Ingrese el tercer número</label>
	        			</div>
	        			<div class="col-12 col-md-6">
	        				<input type="number" id="tercerN" name="tercerN">
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-12 col-md-6 text-center">
	        				<label for="ordenC">El orden decendente es :</label>
	        			</div>
	        			<div class="col-12 col-md-6">
	        				<input type="text" id="ordenC" readonly="readonly" value="<?php echo ordenC();?>">
	        			</div>
	        		</div>
	        		<div class="row">
	        			<div class="col-12 col-md-6 text-center">
	        				<label for="ordenS">El orden decendente es :</label>
	        			</div>
	        			<div class="col-12 col-md-6">
	        				<input type="text" id="ordenS" readonly="readonly" value="<?php echo ordenS();?>">
	        			</div>
	        		</div>
	        		<div class="row mt-3">
						<div class="col-12 text-center">
							<input type="submit" value="Mostrar orden" class="btn btn-dark">
						</div>
					</div>
			</form>
        	</div>
        </section>
        <?php
        	function ordenC()
        	{
        		if (!empty($_POST['primerN']) && !empty($_POST['segundoN']) && !empty($_POST['tercerN'])) {
	        		$num1 = $_POST['primerN'];
		        	$num2 = $_POST['segundoN'];
		        	$num3 = $_POST['tercerN'];
	        		if($num1 > $num2 && $num1 > $num3){
						if($num2 > $num3){
							$txtCexit = $num1.' > '.$num2.' > '.$num3;
						}else{
							$txtCexit = $num1.' > '.$num3.' > '.$num2;
						}
					}else if($num2 > $num3){
						if ($num3 > $num1){
							$txtCexit = $num2.' > '.$num3.' > '.$num1;
						}else{
							$txtCexit = $num2.' > '.$num1.' > '.$num3;
						}
					}else{
						if ($num2 > $num1){
							$txtCexit = $num3.' > '.$num2.' > '.$num1;
						}else{
							$txtCexit = $num3.' > '.$num1.' > '.$num2;
						}
					}
        		}else{
        			$txtCexit = " Ingrese valores";
        		}
        		return $txtCexit;
        	}

        	function ordenS()
        	{
        		if (!empty($_POST['primerN']) && !empty($_POST['segundoN']) && !empty($_POST['tercerN'])){
	        		$num1 = $_POST['primerN'];
		        	$num2 = $_POST['segundoN'];
		        	$num3 = $_POST['tercerN'];
	        		$txtSexit = ' ';
	        		if ($num1 > $num2){
						if ($num1 > $num3){
							if ($num2 > $num3){
								$txtSexit = $num1.' > '.$num2.' > '.$num3;
							}else{
								$txtSexit = $num1.' > '.$num3.' > '.$num2;
							}
						}else{
							$txtSexit = $num3.' > '.$num1.' > '.$num2;
						}
					}else if ($num2 > $num3){
						if ($num3 > $num1){
							$txtSexit = $num2.' > '.$num3.' > '.$num1;
						}else{
							$txtSexit = $num2.' > '.$num1.' > '.$num3;
						}
					}else{
						$txtSexit = $num3.' > '.$num2.' > '.$num1;
					}
        		}else{
        			$txtSexit = " Ingrese valores";
        		}
				return $txtSexit;
        	}
        ?>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
</html>

